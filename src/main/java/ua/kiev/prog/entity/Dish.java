package ua.kiev.prog.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "dish")
@Data
public class Dish {
    @Id
    @GeneratedValue
    private Integer id;
    private String name;
    private int price;
    private int weight;
    private boolean sale;

    public Dish(String name, int price, int weight, boolean sale) {

        this.name = name;
        this.price = price;
        this.weight = weight;
        this.sale = sale;
    }

    public Dish() {

    }
}
