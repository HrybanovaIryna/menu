package ua.kiev.prog.exception;

public class MenuException extends RuntimeException {
    public MenuException(String message) {
        super(message);
    }
}
