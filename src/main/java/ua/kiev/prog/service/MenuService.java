package ua.kiev.prog.service;

import ua.kiev.prog.entity.Dish;

import java.util.List;

public interface MenuService {

    boolean addDish(Dish dish);

    List<Dish> findCostFromTo(int priceFrom, int priceTo);

    List<Dish> findWithSale();

    List<Dish> findWeightLess_1kg();
}
