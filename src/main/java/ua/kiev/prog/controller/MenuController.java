package ua.kiev.prog.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import ua.kiev.prog.dto.Menu;
import ua.kiev.prog.entity.Dish;
import ua.kiev.prog.service.MenuService;

import java.util.List;

@RestController
@RequestMapping("/menu")
@RequiredArgsConstructor
public class MenuController {
    private final MenuService service;

    @PostMapping("/add")
    public boolean addDish(@RequestBody Dish dish) {
        return service.addDish(dish);
    }

    @GetMapping("/get")
    public Menu findCostFromTo(@RequestParam int priceFrom, @RequestParam int priceTo) {
        List<Dish> dishes = service.findCostFromTo(priceFrom, priceTo);
        return getMenu(dishes);
    }

    @GetMapping("/getSale")
    public Menu findWithSale() {
        return getMenu(service.findWithSale());
    }

    @GetMapping("/getWeight")
    public Menu findWeightLess_1kg() {
        return getMenu(service.findWeightLess_1kg());
    }

    private Menu getMenu(List<Dish> dishes) {
        Menu menu = new Menu();
        menu.setDishes(dishes);
        return menu;
    }

}
