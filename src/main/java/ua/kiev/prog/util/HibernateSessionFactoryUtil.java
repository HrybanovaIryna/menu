package ua.kiev.prog.util;

import org.hibernate.SessionFactory;

import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import ua.kiev.prog.dto.Menu;
import ua.kiev.prog.entity.Dish;
import ua.kiev.prog.exception.MenuException;


public class HibernateSessionFactoryUtil {
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Configuration configuration = new Configuration().configure();
                configuration.addAnnotatedClass(Dish.class);
                configuration.addAnnotatedClass(Menu.class);
                StandardServiceRegistryBuilder builder = new StandardServiceRegistryBuilder().applySettings(configuration.getProperties());
                sessionFactory = configuration.buildSessionFactory(builder.build());
            } catch (MenuException e) {
                e.printStackTrace();
            }
        }
        return sessionFactory;
    }
}
