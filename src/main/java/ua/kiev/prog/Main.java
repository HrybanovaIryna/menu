package ua.kiev.prog;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ua.kiev.prog.entity.Dish;
import ua.kiev.prog.service.MenuService;

@SpringBootApplication
@RequiredArgsConstructor
public class Main implements ApplicationRunner {

    private final MenuService menuService;

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Dish porkPie = new Dish("Pork pie", 2, 300, true);
        menuService.addDish(porkPie);
        Dish steakPie = new Dish("Steak pie", 3, 250, false);
        menuService.addDish(steakPie);
        Dish applePie = new Dish("Apple pie", 2, 250, true);
        menuService.addDish(applePie);
        Dish stickyToffeePudding = new Dish("Sticky toffee pudding", 4, 250, false);
        menuService.addDish(stickyToffeePudding);
        Dish saffronCake = new Dish("Saffron cake", 5, 350, false);
        menuService.addDish(saffronCake);
    }
}
