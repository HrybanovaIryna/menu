package ua.kiev.prog.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ua.kiev.prog.entity.Dish;
import ua.kiev.prog.repository.MenuRepository;
import ua.kiev.prog.service.MenuService;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class MenuServiceImpl implements MenuService {

    private static final int TRIES_NUMBER = 20;
    private final MenuRepository repository;

    public boolean addDish(Dish dish) {
        return repository.addDish(dish);
    }

    public List<Dish> findCostFromTo(int priceFrom, int priceTo) {
        return repository.findCostFromTo(priceFrom, priceTo);
    }

    public List<Dish> findWithSale() {
        return repository.findWithSale();
    }

    public List<Dish> findWeightLess_1kg() {
        List<Dish> dishes = new ArrayList<>();
        Random random = new Random();
        List<Dish> allDishes = repository.findAll();
        for (int i = 0; i < TRIES_NUMBER; i++) {
            Dish dish = allDishes.get(random.nextInt(allDishes.size()));
            int currentWeight = getCurrentWeight(dishes);
            if (dish.getWeight() + currentWeight < 1000) {
                dishes.add(dish);
            }
        }
        return dishes;
    }

    private int getCurrentWeight(List<Dish> dishes) {
        int weight = 0;
        for (int i = 0; i < dishes.size(); i++) {
            weight += dishes.get(i).getWeight();
        }
        return weight;
    }
}

