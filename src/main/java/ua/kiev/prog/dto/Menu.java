package ua.kiev.prog.dto;

import lombok.Data;
import ua.kiev.prog.entity.Dish;

import java.util.List;

@Data
public class Menu {

    private List<Dish> dishes;
}
