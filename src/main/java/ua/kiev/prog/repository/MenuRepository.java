package ua.kiev.prog.repository;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import ua.kiev.prog.entity.Dish;
import ua.kiev.prog.util.HibernateSessionFactoryUtil;

import java.util.List;

@Repository
public class MenuRepository {

    public boolean addDish(Dish dish) {
        Session session = null;
        try {
            session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
            session.beginTransaction();
            session.save(dish);
            session.getTransaction().commit();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    public List<Dish> findCostFromTo(int priceFrom, int priceTo) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query<Dish> query = session.createQuery("SELECT dish FROM Dish dish WHERE dish.price >= :priceFrom AND dish.price <= :priceTo");
        query.setParameter("priceFrom", priceFrom);
        query.setParameter("priceTo", priceTo);
        return query.list();
    }

    public List<Dish> findWithSale() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query<Dish> query = session.createQuery("SELECT dish FROM Dish dish where dish.sale = true");
        return query.list();
    }

    public List<Dish> findAll() {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Query<Dish> query = session.createQuery("SELECT dish FROM Dish dish");
        return query.list();
    }
}
